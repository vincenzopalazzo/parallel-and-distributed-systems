#include <iostream>
#include <vector>
#include <functional>
#include <mutex>
#include <atomic>
#include <map>
#include <deque>
#include <thread>
#include <chrono>

#include "aw.cpp"
#include "utimer.cpp"

// some constants to include delays in stages and generator
const bool delay = true;
int  gen_delay = 10;
int  inc_delay = 100;
int  proc_delay = 200;

const bool trace = false;

using namespace std;
using namespace std::literals::chrono_literals;

//
// mange identifiers : rigthmost  4 bits are IID
//                     leftmost  28 bits are GID
//
const int maxIxG = 16;
const int none = -1;

int mkid(int g, int i) {
  if(i>=maxIxG) 
    return(none);
  else 
    return(g*maxIxG+i);
}

int getIid(int id) {
  return(id % maxIxG);
}

int getGid(int id) {
  return(id / maxIxG);
}

//
// instructions
//        operate on vector<int> to get vector<int>
//
using tokt = vector<int>;   // the token type
using InstructionId = int;

class Instruction {
private:
  function<tokt*(tokt)> op;
  tokt * token;
  InstructionId dest;
  
public:

  Instruction(function<tokt*(tokt)> op, InstructionId dest):                // without token
    op(op), dest(dest) {
    token = nullptr;
  }

  Instruction(function<tokt*(tokt)> op, tokt * token, InstructionId dest):  // with token
    token(token),op(op), dest(dest) {}
    
  Instruction() {} 
    
  void setToken(tokt * tok) {              // makes the instruction fireable ... (in the repo)
    token = tok;
    return;
  }
    
  pair<InstructionId, tokt*> compute() {   // computes the instruction and returns computed token and dest id
    // cout << "Before compute.ing token: ";
    // printToken();
    auto res = op(*token);
    // cout <<  "After compute.ing ret token is: "; 
    // for(auto e : *res) cout << e << " "; cout << endl; 
    return(make_pair(dest,res));
  }
    
  bool isFireable() {                      // tells if fireable 
    return(token != nullptr);
  }

  void printToken() {                      // just for debugging
    for(auto e : *token)
      cout << e << " ";
    cout << endl;
    return;
  }
    
};

class Repo {
private:
  map<InstructionId,Instruction> repo;
  deque<InstructionId> fireable; 
  
  mutex m;                   // used to protect against multiple concurrent accesses
  atomic<int> totalI;        // keep track of additional work
  
public:
  Repo() {
    repo.clear();
    fireable.clear();
  }
  
  void setTotal(int i) {
    totalI = i;
    return;
  }

  int getTotal() {
    return(totalI);
  }
  
  void addInstruction(InstructionId id, Instruction i) {
    lock_guard<mutex> l(m);
    repo[id] = i;                // add to repo
    if(i.isFireable())           // in case add to fireable
      fireable.push_back(id);  // aggiornare totalI
    return;
  }
  
  void updateInstructionWithToken(InstructionId id, tokt * tok) {
    if(id == mkid(none,none)) {    // just drain, no need to sink 
      if(trace) {
	cout << "Drain: "; 
	for(auto e : *tok) cout << e << " ";
	cout << endl;
      }
    } else {
      lock_guard<mutex> l(m);
      repo[id].setToken(tok);      // update repo
      fireable.push_back(id);      // add to fireable
    }
    return;
  }
  
  Instruction * getFireable() {
    lock_guard<mutex> l(m);
    if(!fireable.empty()) {
      auto f = fireable.front(); 
      fireable.pop_front();
      totalI--;                    // update total count
      return(&repo[f]);
    }
    return nullptr; 
  }

  void printTokenAt(InstructionId id) {  // just for debugging
    repo[id].printToken();
    return;
  }
    
};

tokt * inc(tokt x) {
  tokt * res = new tokt(x.size());
  for(int i=0; i<x.size(); i++) 
    (*res)[i] = x[i]+1;
  if(delay)
     active_delay(inc_delay);
  return res;
}

tokt * proc(tokt x) {
  tokt * res = new tokt(x.size());
  for(int i=0; i<x.size(); i++) 
    (*res)[i] = (x[i] % 2 == 0 ? x[i]/2 : x[i]);
  if(delay)
    active_delay(proc_delay);
  return res;
}

tokt * gentok(int n, int m) {
  tokt * t = new tokt(n);
  for(int i=0; i<n; i++)
    (*t)[i] = rand()%m;
  if(delay)
    active_delay(gen_delay);
  return(t);
}



  // now working
void genthr(int m, Repo& repo) {
  // gid 
  int g = 0;
  // generate tasks and put in repo
  for(int i=0; i<m; i++) {     // m here is the number of tasks in the input stream
    auto task = gentok(8,8);   // sample input token (should be n,m from command line...)
    if(trace) {
      cout << "Generated task gid = " << g << ": <";
      for(auto e: *task) cout << e << " ";
      cout << ">" << endl;
    }
    repo.addInstruction(mkid(g,0),   // this is already fireable
			Instruction(inc, task, mkid(g,1)));
    // cout << "After adding instruction token is: ";
    // repo.printTokenAt(mkid(g,0));
    repo.addInstruction(mkid(g,1),   // this is not fireable yet
			Instruction(proc, mkid(none,none)));
    g++;     // increase gid, ready for next input stream item
  }
  return;
}



void compthr(int t, Repo& repo) {
  bool end = false;
  int iterno = 0;
  long sleepno = 0;
  long taskno = 0;
  do {
    // cout << "Thread " << t << " iteration " << iterno++ << " (total tasks = " << repo.getTotal() << ")" << endl;
    auto fi = repo.getFireable();
    if(fi != nullptr) {
      // cout << "Thread " << t << " computing token ";
      // fi->printToken();
      auto r = fi->compute();
      // cout << "Thread " << t << " result token is ";
      // for(auto e: *(r.second)) cout << e << " "; cout << endl; 
      repo.updateInstructionWithToken(r.first,(r.second));
      // cout << "Thread " << t << " after updating token is : ";
      // repo.printTokenAt(r.first);
      taskno++; 
    } else {
      if(repo.getTotal() == 0)
	end = true;
      else {
	if(trace) {
	  cout << "@#%$&@## Sleep ..." << endl;       // this is active wait (to be avoided!)
	}
	this_thread::sleep_for(10us);
	sleepno++;
      }
    }
  } while(end != true);
  cout << "Thread " << t << " computed " << taskno << " tasks with " << sleepno << " active waits." << endl; 
  return;
}

int main(int argc, char * argv[]) {

  if(argc == 1) {
    cout << "Usage is " << argv[0] << " seed itemNo pardegree [delay_gen delay_inc delay_proc]" << endl;
    return(-1);
  }
  
  auto seed = atoi(argv[1]);
  auto m    = atoi(argv[2]);
  auto nw   = atoi(argv[3]);

  if(argc == 7) {
    gen_delay = atoi(argv[4]);
    inc_delay = atoi(argv[5]);
    proc_delay = atoi(argv[6]);
  }
  
  // sample instruction and repo demo
  srand(seed);

  Repo repo;

  // this is the small demo ...
  if(false) {
    // add instruction s1
    auto t1 = gentok(8,8);
    cout << "Vector of size " << t1->size() << endl;
    for(auto &e : *t1) cout << e << " ";
    cout << endl; 
    repo.addInstruction(mkid(0,0),Instruction(inc, t1, mkid(0,1)));
    repo.addInstruction(mkid(0,1),Instruction(proc, mkid(none,none)));
    
    auto x = repo.getFireable();
    auto r = x->compute();
    auto dest = r.first;
    cout << "Got result for instruction " << getGid(dest) << "-" << getIid(dest) << endl; 
    auto t2 = r.second;
    cout << "Vector of size " << t2->size() << endl;
    for(auto &e : *t2) cout << e << " ";
    cout << endl; 
    
    repo.updateInstructionWithToken(dest,t2);
    auto y = repo.getFireable();
    auto s = y->compute();
    auto t3 = s.second;
    cout << "Vector of size " << t3->size() << endl;
    for(auto &e : *t3) cout << e << " ";
    cout << endl; 
    auto d = s.first;
    cout << "dest is " << getGid(d) << " - " << getIid(d) << endl;
    
    repo.updateInstructionWithToken(d,t3);
  }

    

  // this is actual main ... 
  srand(seed);
  
  repo.setTotal(m*2);                           // 2 instructions per task (2 stage pipeline)
  vector<thread*> tids(nw);                     //
  long tel; 
  {
    utimer t0("tpar",&tel); 
    auto tgen = new thread(genthr, m, ref(repo)); // generate m random tasks
    for(int i=0; i<nw; i++)                       // generate nw consumer threads
      tids[i] = new thread(compthr, i, ref(repo));
    
    tgen->join();                                 // wait end of generation
    for(auto e : tids)                            // wait the consumer threads
      e->join();
  }
  if(delay) {
    auto tid = m*max(gen_delay, max(inc_delay, proc_delay));
    auto tseq = m * (gen_delay + inc_delay + proc_delay); 
    cout << "Seq     " << tseq << endl; 
    cout << "Ideal   " << tid  << endl;
    cout << "Actual  " << tel/1000  << endl;
    cout << "Speedup " << ((float) tseq)/((float) tel/1000) << endl;
  }
    
  return(0);                                    // that's all folks
}
