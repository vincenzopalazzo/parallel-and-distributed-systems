/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <condition_variable>
#include <mutex>
#include <vector>

template <class T>
class Repository {
 private:
  std::mutex access;
  std::condition_variable wakeup;
  std::vector<std::pair<T, T>> pair_inputs;

 public:
  Repository(std::pair<T, T> elem) : pair_inputs(1) { pair_inputs[0] = elem; }

  Repository() : pair_inputs(0) {}

  Repository(std::vector<std::pair<T, T>> inputs) {
    this->pair_inputs = inputs;
  }

  void add_elem(std::pair<T, T> elem) {
    // Why in the code method there is a code block?
    // It matter, or it is only a good manner?
    std::unique_lock<std::mutex> lock(this->access);
    pair_inputs.push_back(elem);
    wakeup.notify_one();
  }

  bool is_empty() {
    std::unique_lock<std::mutex> lock(this->access);
    auto result = pair_inputs.empty();
    wakeup.notify_one();
    return result;
  }

  std::pair<T, T> pop_back() {
    std::unique_lock<std::mutex> lock(this->access);
    auto elem = *pair_inputs.rbegin();
    wakeup.notify_one();
    return elem;
  }
};
