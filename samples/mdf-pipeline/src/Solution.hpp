/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#ifndef SOL_H
#define SOL_H

#include <cstdlib>
#include <ctime>
#include <random>
#include <vector>
#include <iostream>

#include "Utils.hpp"

const cpstl::Log LOG(false);

template <typename T, typename R>
static std::vector<R> makeInput(std::pair<T, T> input) {
  std::random_device rd;
  std::mt19937_64 mt(rd());

  auto n = input.first;
  auto m = input.second;
  std::vector<R> result(n);
  auto generator = std::uniform_real_distribution<T>(0, m);
  for (auto &elem : result) elem = generator(mt);
  return result;
}

template <typename T>
static void stageOne(std::vector<T> &inputs, std::size_t const &workers,
                     bool parallel) {
  if (parallel) {
#pragma omp parallel for  num_threads(workers)
    for (auto &elem : inputs) elem++;
    return;
  }
  for (auto &elem : inputs) elem++;
}

template <typename T>
static void stageTwo(std::vector<T> &inputs, std::size_t const &workers,
                     bool parallel) {
  // TODO(vincenzopalazzo): having each element.
  if (parallel) {
#pragma omp parallel for num_threads(workers)
    for (auto &elem : inputs) {
      if (elem > 0) elem /= 2;
    }
    return;
  }

  for (auto &elem : inputs) {
    if (elem > 0) elem /= 2;
  }
}

template <typename T>
static void endStage(std::vector<T> &input) {
  std::cout << "[ ";
  for (std::size_t i = 0; i < input.size(); i++) {
    if (i == (input.size() - 1)) {
      std::cout << i << " ]\n";
    } else {
      std::cout << i << ", ";
    }
  }
}
#endif
