/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <cassert>
#include <random>
#include <thread>

#include "Repository.hpp"
#include "Solution.hpp"
#include "Worker.hpp"
#include "cmdline/cmdline.h"
#include "utimer.hpp"

cmdline::parser configCommandLine(int argc, char *argv[]) {
  cmdline::parser cmd;
  cmd.add("version", 'v', "Print version program");
  cmd.add<int>("input", 'i', "Set the pair number that you want insert", true,
               0);
  cmd.add<int>("workers", 'w', "Numbers of workers", true, 0);
  cmd.add<int>("max", 'm', "Max num of the range", true, 100);
  cmd.add<bool>("parallel", 'p', "Run the single stage in parallel", false, false);
  cmd.parse_check(argc, argv);
  return cmd;
}

int main(int argc, char *argv[]) {
  cmdline::parser cmd = configCommandLine(argc, argv);
  auto size = cmd.get<int>("input");
  auto workers = cmd.get<int>("workers");
  auto num = cmd.get<int>("max");
  auto parallel = cmd.get<bool>("parallel");

  std::cout << "Size input " << size << "\n";
  std::cout << "With " << workers  << " workers\n";
  std::cout << "Range from 0 to " << num << "\n";
  std::cout << "Parallel enable " << parallel << "\n";

  Worker<float> worker(workers, parallel);

  std::random_device rd;
  std::mt19937_64 mt(rd());
  auto generator = std::uniform_real_distribution<float>(1, num);

  {
    utimer timer("pipeline");
    for (std::size_t i = 0; i < size; i++) {
      float x, y;
      x = generator(mt);
      y = generator(mt);
      std::pair<float, float> p(x, y);
      worker.push_back(p);
    }

    /*
    for (std::size_t i = 0; i < size; i++) {
      float x, y;
      std::cout << "Insert the pair: ";
      std::cin >> x >> y;
      std::cout << "The pair are: X=" << x << ", Y=" << y << "\n";
      auto tmp = std::pair<float, float>(x, x);
      worker.push_back(tmp);
    }
    */

    worker.tear_down();
  }
  return EXIT_SUCCESS;
}
