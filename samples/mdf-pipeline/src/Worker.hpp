/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <atomic>
#include <thread>
#include <vector>

#include "Solution.hpp"
#include "Utils.hpp"

template <typename T>
void event_chain(std::pair<T, T> pair, Repository<T> &repo,
                 std::size_t &max_worker, std::atomic<std::size_t> &working,
                 bool parallel) {
  while (max_worker <= working.load()) {
    cpstl::cp_log(LOG, "Too many workers waiting");
  }

  working++;
  //std::cout << "Runnin Threads " << working.load() << "\n"; // TODO: This require the lock on the variable, it is dangerus
  while(repo.is_empty()) {
    cpstl::cp_log(LOG, "Repo empty");
  }

  auto elem = repo.pop_back();
  auto randVector = makeInput<T, T>(elem);
  stageOne<T>(randVector, max_worker, parallel);
  stageTwo<T>(randVector, max_worker, parallel);
  // endStage<T>(randVector);

  working--;
}

template <class T>
class Worker {
 private:
  Repository<T> repository;
  std::size_t workers;
  std::atomic<std::size_t> working;
  std::vector<std::thread> threads;
  bool parallel;

 public:
  Worker(std::size_t workers, bool parallel) :
    workers(workers), working(0), parallel(parallel) {}

  void push_back(std::pair<T, T> pair) {
    this->repository.add_elem(pair);
    std::thread th(event_chain<T>, pair, std::ref(repository),
                   std::ref(workers), std::ref(working),
                   parallel);

    threads.push_back(std::move(th));
  }

  void tear_down() {
    for (auto &th : threads) th.join();

    cpstl::cp_log(LOG, "Finished");
  }
};
