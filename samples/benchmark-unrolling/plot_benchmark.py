import json
import plotly.graph_objects as go


def plot_result():
    result_json = open("result.json", "r")
    bm_json = json.loads(result_json.read())

    labels = []
    unrolled = []
    rolled = []
    for bm in bm_json["benchmarks"]:
        if "BM_UNROLLED" in bm["name"]:
            labels.append("{}".format(bm["run_name"].split("/")[1]))
            unrolled.append(bm["real_time"])
        elif "BM_NOT_UNROLLED" in bm["name"]:
            rolled.append(bm["real_time"])

    fig = go.Figure()
    fig.add_trace(
        go.Scatter(
            x=labels,
            y=unrolled,
            mode="lines+markers",
            name="UNROLLED",
            line_color="rgb(153, 84, 187)",
        )
    )
    fig.add_trace(
        go.Scatter(
            x=labels,
            y=rolled,
            mode="lines+markers",
            name="ROLLED",
            line_color="rgb(39, 128, 227)",
        )
    )

    fig.show()


if __name__ == "__main__":
    plot_result()
