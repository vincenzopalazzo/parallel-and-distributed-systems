/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <algorithm>
#include <ctime>

#include <benchmark/benchmark.h>
#include "Solution.hpp"

static void BM_UNROLLED(benchmark::State& state){
  // Usualy the function takes the inputs array
  std::srand(unsigned(std::time(nullptr)));
  for (auto _ : state) {
    state.PauseTiming();
    std::vector<int> inputs(state.range(0));
    std::generate(inputs.begin(), inputs.end(), std::rand);
    state.ResumeTiming();
    unrolled_forloop(inputs);
  }
}

static void BM_NOT_UNROLLED(benchmark::State& state) {
  std::srand(unsigned(std::time(nullptr)));
  for (auto _ : state) {
    state.PauseTiming();
    std::vector<int> inputs(state.range(0));
    std::generate(inputs.begin(), inputs.end(), std::rand);
    state.ResumeTiming();
    not_unrolled_forloop(inputs);
  }
}

BENCHMARK(BM_UNROLLED)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);
BENCHMARK(BM_NOT_UNROLLED)->Arg(8)->Arg(64)->Arg(512)->Arg(1<<10)->Arg(8<<10);

BENCHMARK_MAIN();
