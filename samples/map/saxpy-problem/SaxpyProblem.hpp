/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <vector>

template <typename T>
void saxpy_serial_sequential(std::vector<T> const &inputs, std::vector<T> &output, T scale_factor) {
  for (std::size_t i = 0; i < inputs.size(); ++i) {
    output[i] = scale_factor * inputs[i] + output[i];
  }
}

template<typename T>
void saxpy_pure_parallel_map_implementation(std::vector<T> const &inputs,
                                            std::vector<T> &outputs,
                                            T scale_factor,
                                            std::size_t workers) {
#pragma omp parallel for num_threads(workers)
  for(std::size_t i = 0; i < inputs.size(); ++i) {
    outputs[i] = scale_factor * inputs[i] + outputs[i];
  }
}
