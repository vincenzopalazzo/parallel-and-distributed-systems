/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <chrono>
#include <benchmark/benchmark.h>

#include "GenericScopes.hpp"

static void BM_solution_one(benchmark::State& state){
  // Usualy the function takes the inputs array
  for (auto _ : state) {
    state.PauseTiming();
    auto size_array = state.range(0);
    auto waitingTime = 100; // FIXME(vincenzopalazzo) This need to be fixed, see below
    state.ResumeTiming();
    GenericStage<int> genericScoper;
    genericScoper.run(size_array, std::chrono::milliseconds(waitingTime));
  }
}

static void BM_solution_one_sequential(benchmark::State& state) {
  for (auto _ : state) {
    state.PauseTiming();
    auto sizeArray = state.range(0);
    auto waitingTime = 100; //FIXME(vincenzopalazzo) This need to be fixed, use another type of range with google library
    state.ResumeTiming();
    GenericStage<int> genericStage;
    genericStage.runSequential(sizeArray, std::chrono::milliseconds(waitingTime));
  }
}

BENCHMARK(BM_solution_one)->RangeMultiplier(10)->Range(10, 1000);;
BENCHMARK(BM_solution_one_sequential)->RangeMultiplier(10)->Range(10, 1000);

BENCHMARK_MAIN();
