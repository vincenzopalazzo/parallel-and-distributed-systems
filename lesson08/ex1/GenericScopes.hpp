/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <chrono>
#include <thread>
#include <cassert>

#include "./includes/queue.hpp"

using namespace std;
using namespace chrono;

template <typename T>
class GenericStage {

private:
  myqueue<T> sourceToGeneric;
  myqueue<T> genericToDrain;

  void source(std::size_t dimensionArray, milliseconds time) {
    assert((dimensionArray > 0, "Dimension array is wrong"));
    for (std::size_t i = 0; i < dimensionArray; i++) {
      std::this_thread::sleep_for(time);
      this->sourceToGeneric.push(i);
    }
    this->sourceToGeneric.push(EOS);
  }

  void drain(milliseconds time){
    auto element = this->genericToDrain.pop();

    while(element != EOS) {
      std::this_thread::sleep_for(time);
      element = this->genericToDrain.pop();
    }
  }

  void genericStages(milliseconds time) {
    auto element = this->sourceToGeneric.pop();

    while(element != EOS) {
      std::this_thread::sleep_for(time);
      element++;
      this->genericToDrain.push(element);
      element = this->sourceToGeneric.pop();
    }
    this->genericToDrain.push(EOS);
  }
public:

  void run(std::size_t dimensionArray, milliseconds waitTime) {
    thread stageOne(&GenericStage::source, this, std::ref(dimensionArray),
                    std::ref(waitTime));
    thread stageTwo(&GenericStage::genericStages, this, std::ref(waitTime));
    thread stageThree(&GenericStage::drain, this, std::ref(waitTime));

    stageOne.join();
    stageTwo.join();
    stageThree.join();
  }

  void runSequential(std::size_t dimensionArray, milliseconds waitTime) {
    this->source(dimensionArray, waitTime);
    this->genericStages(waitTime);
    this->drain(waitTime);
  }
};
