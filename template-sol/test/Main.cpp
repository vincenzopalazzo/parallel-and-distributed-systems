/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <iostream>
#include <algorithm>

#include "../cmdline/cmdline.h"

#include "../includes/utimer.hpp"
#include "../SaxpyProblem.hpp"
#include "TestTool.hpp"
#include "Utils.hpp"

/**
 * This method make a copy of the input and make the analysis in the local machine.
 * The copy help to work on the same array.
 */
template <typename T>
std::vector<T> pure_single_thread(std::vector<T> &inputs, T scale_factor, long &usec) {
  std::vector<T> output(inputs.size());
  {
    utimer timer("Single Thread version", &usec);
    saxpy_serial_sequential(inputs, output, scale_factor);
  }
  return output;
}

template<typename T>
std::vector<T> openmp_version(std::vector<T> &inputs, T scale_factors,
                                    std::size_t workers, long &usec)
{
  std::vector<T> outputs(inputs.size());
  {
    utimer timer("OpenMP version", &usec);
    saxpy_pure_parallel_map_implementation<T>(inputs, outputs, scale_factors, workers);
  }
  return outputs;
}

const cpstl::Log LOG(true);

int main(int argc, char *argv[]) {

  cmdline::parser commands;
  commands.add<int>("threads", 't', "Specify the threads that you want use", false, 1);
  commands.add<int>("input-size", 'i', "Specify the dimension of the input array that need to be sorted", true, 100);
  commands.parse_check(argc, argv);

  std::srand(unsigned(std::time(nullptr)));
  auto size_array = commands.get<int>("input-size");
  // std::cout << "Size array = " << size_array << "\n";
  std::vector<float> inputs(size_array);
  std::generate(inputs.begin(), inputs.end(), std::rand);
  long single_ex = 0.0;
  auto result = pure_single_thread<float>(inputs, 1.2, single_ex);

  auto worker = commands.get<int>("threads");
  //std::cout << "Workers = " << worker << "\n";
  long openmp_ex = 0.0;
  auto result_parallel = openmp_version<float>(inputs, 1.2, worker, openmp_ex);
  cpstl::assert_equal("", result, result_parallel);

  auto openmp_speedup = ((float) single_ex) / ((float) openmp_ex) ;
  cpstl::cp_log(LOG, "OpenMP Speedup " + std::to_string(openmp_speedup));
  return EXIT_SUCCESS;
}
