cmake_minimum_required(VERSION 3.9)
project(SaxpyProblem)

set(CMAKE_CXX_STANDARD 14)

find_package(OpenMP REQUIRED)

add_executable(
        ${PROJECT_NAME}
        test/Main.cpp
)

target_link_libraries(${PROJECT_NAME} PRIVATE OpenMP::OpenMP_CXX)
