## Parallel and Distributed Systems Make template

This template include all the necessary to build an application of the Parallel and Distributed courser.

It include the following feature

- [X] Run the app with jemalloc with the help of command `make jemalloc`
- [X] Include a real command line parser to make the things more coller.
- [X] use variable in make file to change the complier and optization code, by default the optimization code is equal to 3 and the compiler is g++
- [X] Clang formart of the source directory


## Starting with the template.

make the following command to start

```bash
git clone --recurse-submodules https://gitlab.com/vincenzopalazzo/parallel-and-distributed-systems.git
``

## Project that use this template.

TODO: 

## License

TODO
