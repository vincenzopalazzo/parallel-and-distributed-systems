/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <atomic>
#include <thread>
#include <vector>

template <typename T>
void compute_parallel_for(std::vector<T> &inputs, std::atomic<bool> &is_sorted,
                          T start, T end, int workers) {
#pragma omp parallel for num_threads(workers)
  for (std::size_t i = start; i < end - 1; i += 2) {
    if (inputs[i] > inputs[i + 1]) {
#pragma omp critical
      {
        is_sorted = false;
        std::swap(inputs[i], inputs[i + 1]);
      }
    }
  }
  return;
}

template <typename T>
void odd_even_sort_openmp(std::vector<T> &inputs, T start, T end, int workers) {
  std::atomic<bool> is_sorted;
  is_sorted = false;

  while (!is_sorted.load()) {
    is_sorted = true;

    int start_even;
    int start_odd;
    if (start % 2) {
      start_even = start;
      start_odd = start + 1;
    } else {
      start_even = start + 1;
      start_odd = start;
    }

    std::thread oddWorker(compute_parallel_for<T>, std::ref(inputs),
                          std::ref(is_sorted), std::ref(start_even),
                          std::ref(end), std::ref(workers));
    std::thread evenWorker(compute_parallel_for<T>, std::ref(inputs),
                           std::ref(is_sorted), std::ref(start_odd),
                           std::ref(end), std::ref(workers));

    oddWorker.join();
    evenWorker.join();
  }
}
