/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <algorithm>
#include <cassert>
#include <cstdlib>
#include <vector>

#include "Order.hpp"
#include "Solution.hpp"
#include "cmdline/cmdline.h"
#include "includes/utimer.hpp"

/**
 * This method make a copy of the input and make the analysis in the local
 * machine. The copy help to work on the same array.
 */
template <typename T>
std::vector<T> sort_pure_single_thread(std::vector<T> inputs, long &usec) {
  {
    utimer timer("Single Thread version", &usec);
    cpstl::odd_even_sort(inputs, 0, inputs.size() - 1);
  }
  return inputs;
}

template <typename T>
std::vector<T> sort_openmp(std::vector<T> inputs, int worker, long &usec) {
  {
    utimer time("OpenMP version", &usec);
    odd_even_sort_openmp(inputs, 0, (int)inputs.size() - 1, worker);
  }
  return inputs;
}

int main(int argc, char *argv[]) {
  cmdline::parser commands;
  commands.add<int>("threads", 't', "Specify the threads that you want use",
                    false, 1);
  commands.add<int>(
      "input-size", 'i',
      "Specify the dimension of the input array that need to be sorted", true,
      100);

  commands.parse_check(argc, argv);

  std::srand(unsigned(std::time(nullptr)));
  auto size_array = commands.get<int>("input-size");
  std::cout << "Size array = " << size_array << "\n";
  std::vector<int> inputs(size_array, 0);
  std::generate(inputs.begin(), inputs.end(), std::rand);
  auto worker = commands.get<int>("threads");
  std::cout << "Workers = " << worker << "\n";
  long sequential = 0;
  long openmp = 0;

  auto sorted_pure_single = sort_pure_single_thread<int>(inputs, sequential);
  auto sort_with_openmp = sort_openmp(inputs, worker, openmp);
  for (std::size_t i = 0; i < inputs.size(); i++) {
    if (sort_with_openmp[i] != sorted_pure_single[i])
      assert(false && "Sort result different");
  }

  std::cout << "Speed Up with OpenMP: " << ((float)sequential) / ((float)openmp)
            << "\n";

  return EXIT_SUCCESS;
}
