#include <iostream>
#include <cstdlib>
#include <algorithm>
#include <vector>
#include <chrono>
#include <mutex>
#include <thread>
#include <string>

using namespace std;

#include "utimer.cpp"

template<typename T>
void pv(string m, vector<T> v) {
#ifdef PRINT
  std::cout << m << std::endl;
  for(auto &e : v) std::cout << e << " ";
  std::cout << std::endl;
#endif
}

int main(int argc, char * argv[]) {

  int seed = atoi(argv[1]);
  int n    = atoi(argv[2]);
  int nw   = atoi(argv[3]);
  
  const int bn = 256;

  vector<int> image(n);
  srand(seed);
  generate(image.begin(), image.end(),
	   []() { return(rand()%bn); });

  

  pv<int>("image", image); 
  vector<int> histo(bn,0);

  long tseq;
  {
    utimer ts("Tseq",&tseq); 
    for_each(image.begin(), image.end(),
	     [&] (unsigned char i) { histo[i]++; });
  }

  pv<int>("histo", histo);
  
  vector<int> histo_thr(bn,0);

  vector<vector<int>> locals(nw);

  auto thread_body = [&] (int threadno) {
		       // compute range
		       auto delta = n / nw;
		       auto start = (threadno * delta);
		       auto stop  = (threadno == (nw-1) ? n : (threadno+1) * delta);
		       // alloc locals
		       locals[threadno].resize(bn,0);
		       // compute histogram
		       // cout << "thread " << threadno << " working on " << start << "," << stop << endl; 
		       for(int i=start; i<stop; i++) {
			 auto v = image[i];
			 locals[threadno][v]++;
		       }
		       return; 
		     };
  
  vector<thread*> tids(nw);
  long tpar;
  {
    utimer to("Tpar",&tpar);
    for(int i=0; i<nw; i++)
      tids[i] = new thread(thread_body, i);
    for(int i=0; i<nw; i++)
      tids[i]->join();
    // after finishing, merge locals
    for(int j=0; j<bn; j++) {
      for(int i=0; i<nw; i++) {
	histo_thr[j] += locals[i][j];
      }
    }
  }

  for(int i=0; i<nw; i++)
    pv<int>("local", locals[i]);
	    
  pv<int>("histo_omp", histo_thr);
  int maxn = 10;
  for(int i=0; i<bn; i++)
    if(histo[i] != histo_thr[i]) {
      std::cerr << "Histograms differ (at " <<
	i << " " << histo[i] << " vs " << histo_thr[i] << ")" << std::endl;
      if(maxn-- == 0)
	break;
    }
  

  std::cout << "Computed histogram with speedup " << ((float) tseq)/((float) tpar) << std::endl;
  
  return(0);
}
