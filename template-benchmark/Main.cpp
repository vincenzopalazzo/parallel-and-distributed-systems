/**
 * Solutions and benchmarks about problem resolved for the class
 * SPM 20/21 Parallel and distributed system.
 *
 * Copyright (C) 2021  Vincenzo Palazzo <vincenzopalazzodev@gmail.com>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301,
 * USA.
 */
#include <benchmark/benchmark.h>
#include "Solution.hpp"

static void BM_solution_one(benchmark::State& state){
  // Usualy the function takes the inputs array
  for (auto _ : state) {
    state.PauseTiming();
    std::vector<int> inputs;
    for (int i = 0; i < state.range(0); i++) {
      inputs.push_back(i);
    }
    state.ResumeTiming();
    Solution<int> sol;
    sol.asssignAndPrint();
  }
}

BENCHMARK(BM_solution_one)->DenseRange(10, 10000, 100);

BENCHMARK_MAIN();
